#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

void delete_ending_newlines(char* str){
  int i;

  for(i=strlen(str)-1;i>=0;i--){
    if(str[i] == '\n' || str[i] == '\r')
      str[i] = '\0';
  }
}

main(){
  int png_fd = open("./void.png", O_RDONLY);
  const char* addr = getenv("REMOTE_ADDR");
  const char* item_name = getenv("QUERY_STRING");
  char buff[1024], host[1024], log_name[1024];
  
  // Error: the dummy image is not there
  if(png_fd == -1){
    printf("Content-type: text/plain\n\n"); // there must be TWO '\n's
    printf("Error: open() returned -1\n");

    return 0;
  }
  else{
    int n_bytes;
    const char* header = "Content-type: image/png\n\n";
    char command_host[128], command_geoip[128];
    char timestr[128], geostr[128];
    FILE* log = NULL;
    time_t now;

    // get current time as a string
    now = time(NULL);
    ctime_r(&now, timestr);
    delete_ending_newlines(timestr);

    // open the log file
    sprintf(log_name, "./logs/%s.txt", item_name);
    log = fopen(log_name, "a");

    // get hostname
    sprintf(command_host, "host %s | cut -d ' ' -f 5", addr);
    FILE* pipe_host = popen(command_host, "r");
    fgets(host, sizeof(host), pipe_host);
    delete_ending_newlines(host);

    // get geo info
    sprintf(command_geoip, "geoiplookup %s | cut -d ' ' -f 4-", addr);
    FILE* pipe_geoip = popen(command_geoip, "r");
    fgets(geostr, sizeof(geostr), pipe_geoip);
    delete_ending_newlines(geostr);

    // show the dummy image
    write(STDOUT_FILENO, header, strlen(header));

    while((n_bytes = read(png_fd, buff, sizeof(buff))) != 0){
      write(STDOUT_FILENO, buff, n_bytes);
    }
    
    // write the log
    fprintf(log, "[%s] %s %s (%s)\n", timestr, addr, host, geostr);

    close(png_fd);
    fclose(log);
    pclose(pipe_host);

    return 0;
  }
}
